[![](https://jitpack.io/v/com.gitlab.aguragorn/klog.svg)](https://jitpack.io/#com.gitlab.aguragorn/klog)

# Klog
Centralized logger for multiple logging implementations in your multiplatform code.

## Installation
1. Add jitpack to repositories in your root build.gradle

    **Gradle DSL**
    ```
    allprojects {
        repositories {
            ...
            maven { url 'https://jitpack.io' }
        }
    }
    ```
   **Kotlin DSL**
   ```
   allprojects {
       repositories {
           ...
           maven { url = uri("https://jitpack.io") }
       }
   }
   ```
2. Add the dependency
    
    **Gradle DSL**
    ```
    dependencies {
        implementation 'com.gitlab.aguragorn.klog:KLog:$VERSION'
    }
    ```
   **Kotlin DSL**
   ```
   dependencies {
        implementation("com.gitlab.aguragorn.klog:KLog:$VERSION")
   }
   ```
## Usage

### Implement Logger (OPTIONAL)

A `LogWriter` formats and writes the logs into your preferred output destination ie. logcat, console, etc.  

```
class CustomLogWriter : LogWriter {

        override fun d(tag: String, msg: () -> String, throwable: Throwable?) {
            // implement debug logging here
        }

        override fun i(tag: String, msg: () -> String, throwable: Throwable?) {
            // implement info logging here
        }

        override fun w(tag: String, msg: () -> String, throwable: Throwable?) {
            // implement warn logging here
        }

        override fun e(tag: String, msg: () -> String, throwable: Throwable?) {
            // implement error logging here
        }

        override fun wtf(tag: String, msg: () -> String, throwable: Throwable?) {
            // wtf log level is reserved for events that are not exepected to happen
        }

    }
```
### Initialize logging
```
// logLevel - the over all lowest LogLevel allowed across all loggers
Log.init(logLevel) {
    
    // register a log writer
    register(CustomLogWriter())
    
    // - turn off logging for all log levels of logs
    //   with tag "TAG_TO_DISABLE"
    deny("TAG_TO_DISABLE", LogLevel.WTF)

    // - allow only LogLevel.INFO or higher of logs
    //   with tag "TAG_TO_ENABLE"
    // - does not override the over all log level
    allow("TAG_TO_ENABLE", LogLevel.INFO)
}
```
or just
```
Log.init(logLevel)
```