package com.aguragorn.klog

class Logger(
    private val name: String
) {
    fun d(throwable: Throwable? = null, msg: () -> String) {
        Log.d(name, msg, throwable)
    }

    fun d(throwable: Throwable) {
        Log.d(name, { throwable.messageOrClassName }, throwable)
    }

    fun i(throwable: Throwable? = null, msg: () -> String) {
        Log.i(name, msg, throwable)
    }

    fun i(throwable: Throwable) {
        Log.i(name, { throwable.messageOrClassName }, throwable)
    }

    fun w(throwable: Throwable? = null, msg: () -> String) {
        Log.w(name, msg, throwable)
    }

    fun w(throwable: Throwable) {
        Log.w(name, { throwable.messageOrClassName }, throwable)
    }

    fun e(throwable: Throwable? = null, msg: () -> String) {
        Log.e(name, msg, throwable)
    }

    fun e(throwable: Throwable) {
        Log.e(name, { throwable.messageOrClassName }, throwable)
    }

    fun wtf(throwable: Throwable? = null, msg: () -> String) {
        Log.wtf(name, msg, throwable)
    }

    fun wtf(throwable: Throwable) {
        Log.wtf(name, { throwable.messageOrClassName }, throwable)
    }

    companion object {
        fun withName(name: String): Logger {
            return Logger(name)
        }
    }
}