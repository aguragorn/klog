package com.aguragorn.klog

enum class LogLevel(val description: String, val rawValue: Int) {
    DEBUG("debug", 0),
    INFO("info", 1),
    WARNING("warning", 2),
    ERROR("error", 3),
    WTF("what the f*ck is this?!", 4);

    operator fun unaryPlus() = of(rawValue + 1)
    operator fun unaryMinus() = of(rawValue - 1)

    companion object {
        val default = INFO

        fun of(rawValue: Int) = when {
            rawValue < 0 -> DEBUG
            rawValue > 4 -> WTF
            else -> values().find { it.rawValue == rawValue } ?: default
        }

    }
}