package com.aguragorn.klog

import co.touchlab.stately.isolate.IsolateState

object Log {
    private val loggers = IsolateState { mutableSetOf<LogWriter>() }
    private val denyList = IsolateState { mutableMapOf<String, LogLevel>() }
    @Suppress("MemberVisibilityCanBePrivate")
    var currentLevel: LogLevel by Atomic(LogLevel.default)

    @Suppress("Unused")
    fun register(writer: LogWriter) {
        loggers.access { it.add(writer) }
    }

    fun setWriters(writers: Set<LogWriter>) {
        loggers.access {
            it.clear()
            it.addAll(writers)
        }
    }

     fun d(tag: String, msg: () -> String, throwable: Throwable? = null) {
        log(LogLevel.DEBUG, tag, msg, throwable)
    }

     fun i(tag: String, msg: () -> String, throwable: Throwable? = null) {
        log(LogLevel.INFO, tag, msg, throwable)
    }

     fun w(tag: String, msg: () -> String, throwable: Throwable? = null) {
        log(LogLevel.WARNING, tag, msg, throwable)
    }

     fun e(tag: String, msg: () -> String, throwable: Throwable? = null) {
        log(LogLevel.ERROR, tag, msg, throwable)
    }

     fun wtf(tag: String, msg: () -> String, throwable: Throwable? = null) {
        log(LogLevel.WTF, tag, msg, throwable)
    }

    fun init(level: LogLevel, block: Log.() -> Unit = {}) {
        denyList.access { it.clear() }
        loggers.access { it.clear() }
        currentLevel = level
        setWriters(setOf(PlatformDefaultWriter()))
        this.block()
    }

    /**
     * Prevents logging of [tag]s for the specified [logLevel] or lower.
     * If no [LogLevel] is specified it is set to the highest log level,
     * [LogLevel.WTF], meaning all logs are blocked
     */
    fun deny(tag: String, logLevel: LogLevel = LogLevel.WTF) {
        denyList.access { it[tag] = logLevel }
    }

    /**
     * Allows logging of [tag]s for the specified [logLevel] or higher.
     * If no [LogLevel] is specified it is set to the lowest log level,
     * [LogLevel.DEBUG], meaning all logs are allowed
     */
    fun allow(tag: String, logLevel: LogLevel = LogLevel.DEBUG) {
        if (logLevel == LogLevel.DEBUG) {
            denyList.access { it.remove(tag) }
        } else {
            // only block lower LogLevels
            denyList.access { it[tag] = -logLevel }
        }
    }

    private fun log(logLevel: LogLevel, tag: String, msg: () -> String, throwable: Throwable? = null) {
        if (logLevel.rawValue < currentLevel.rawValue) return

        val denyListEntry = denyList.access { it[tag] }
        if (denyListEntry != null && logLevel <= denyListEntry) return

        when (logLevel) {
            LogLevel.DEBUG -> loggers.access {
                for (logger in it) {
                    logger.d(tag, msg, throwable)
                }
            }

            LogLevel.INFO -> loggers.access {
                for (logger in it) {
                    logger.i(tag, msg, throwable)
                }
            }

            LogLevel.WARNING -> loggers.access {
                for (logger in it) {
                    logger.w(tag, msg, throwable)
                }
            }

            LogLevel.ERROR -> loggers.access {
                for (logger in it) {
                    logger.e(tag, msg, throwable)
                }
            }

            LogLevel.WTF -> loggers.access {
                for (logger in it) {
                    logger.wtf(tag, msg, throwable)
                }
            }
        }
    }
}