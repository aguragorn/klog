package com.aguragorn.klog

import co.touchlab.stately.concurrency.AtomicReference
import kotlin.reflect.KProperty

val Any.TAG get() = this::class.simpleName ?: ""

val Throwable.messageOrClassName get() = message ?: this::class.simpleName ?: "Unknown Error"

fun String.withTopic(vararg topic: String): String = "[$this]${topic.joinToString(separator = "") { "[$it]" }}"

internal class Atomic<T>(initialValue: T) {
    private val atomicVal = AtomicReference(initialValue)

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return atomicVal.get()
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        atomicVal.set(value)
    }
}