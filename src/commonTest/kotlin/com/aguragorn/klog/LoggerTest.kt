package com.aguragorn.klog

import co.touchlab.stately.concurrency.AtomicReference
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class LoggerTest {
    private var logWriter = AtomicReference(TestLogWriter(withStackTrace = false))
    private val msg = "msg"
    private val logger = Logger.withName("LoggerTest")

    @BeforeTest
    fun setUp() {
        logWriter.set(TestLogWriter(withStackTrace = false))

        Log.init(LogLevel.DEBUG) {
            setWriters(setOf(logWriter.get()))
        }
    }

    @Test
    fun `log exception message when msg is not provided`() {
        logger.d(Exception(msg))
        assertEquals(
            expected = "d/LoggerTest $msg",
            actual = logWriter.get().lastLog
        )
        logger.i(Exception(msg))
        assertEquals(
            expected = "i/LoggerTest $msg",
            actual = logWriter.get().lastLog
        )
        logger.w(Exception(msg))
        assertEquals(
            expected = "w/LoggerTest $msg",
            actual = logWriter.get().lastLog
        )
        logger.e(Exception(msg))
        assertEquals(
            expected = "e/LoggerTest $msg",
            actual = logWriter.get().lastLog
        )
        logger.wtf(Exception(msg))
        assertEquals(
            expected = "wtf/LoggerTest $msg",
            actual = logWriter.get().lastLog
        )
    }

    @Test
    fun `log exception class name when msg is not provided and Exception does not have a message`() {
        logger.d(Exception())
        assertEquals(
            expected = "d/LoggerTest Exception",
            actual = logWriter.get().lastLog
        )
        logger.i(Exception())
        assertEquals(
            expected = "i/LoggerTest Exception",
            actual = logWriter.get().lastLog
        )
        logger.w(Exception())
        assertEquals(
            expected = "w/LoggerTest Exception",
            actual = logWriter.get().lastLog
        )
        logger.e(Exception())
        assertEquals(
            expected = "e/LoggerTest Exception",
            actual = logWriter.get().lastLog
        )
        logger.wtf(Exception())
        assertEquals(
            expected = "wtf/LoggerTest Exception",
            actual = logWriter.get().lastLog
        )
    }
}