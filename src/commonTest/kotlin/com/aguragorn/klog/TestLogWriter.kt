package com.aguragorn.klog


class TestLogWriter(
    private val withStackTrace: Boolean = true
) : LogWriter {
    var lastLog: String? by Atomic(null)

    override fun d(tag: String, msg: () -> String, throwable: Throwable?) {
        val stackTrace = throwable
            ?.takeIf { withStackTrace }
            ?.let { ": ${it.stackTraceToString()}" }.orEmpty()
        lastLog = "d/$tag ${msg()}$stackTrace"
    }

    override fun i(tag: String, msg: () -> String, throwable: Throwable?) {
        val stackTrace = throwable
            ?.takeIf { withStackTrace }
            ?.let { ": ${it.stackTraceToString()}" }.orEmpty()
        lastLog = "i/$tag ${msg()}$stackTrace"
    }

    override fun w(tag: String, msg: () -> String, throwable: Throwable?) {
        val stackTrace = throwable
            ?.takeIf { withStackTrace }
            ?.let { ": ${it.stackTraceToString()}" }.orEmpty()
        lastLog = "w/$tag ${msg()}$stackTrace"
    }

    override fun e(tag: String, msg: () -> String, throwable: Throwable?) {
        val stackTrace = throwable
            ?.takeIf { withStackTrace }
            ?.let { ": ${it.stackTraceToString()}" }.orEmpty()
        lastLog = "e/$tag ${msg()}$stackTrace"
    }

    override fun wtf(tag: String, msg: () -> String, throwable: Throwable?) {
        val stackTrace = throwable
            ?.takeIf { withStackTrace }
            ?.let { ": ${it.stackTraceToString()}" }.orEmpty()
        lastLog = "wtf/$tag ${msg()}$stackTrace"
    }

}