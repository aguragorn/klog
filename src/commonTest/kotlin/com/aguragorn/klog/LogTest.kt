package com.aguragorn.klog

import co.touchlab.stately.concurrency.AtomicReference
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class LogTest {
    private val tag = TAG
    private var testLogger = AtomicReference(TestLogWriter())
    private val msg = "msg"

    @BeforeTest
    fun setUp() {
        testLogger.set(TestLogWriter())

        Log.init(LogLevel.DEBUG) {
            setWriters(setOf(testLogger.get()))
        }
    }

    @Test
    fun no_debug_logs_when_debug_is_denied_for_tag() {
        Log.deny(tag, LogLevel.DEBUG)

        Log.d(tag, { msg })
        assertEquals(null, testLogger.get().lastLog)
        Log.i(tag, { msg })
        assertEquals("i/$tag $msg", testLogger.get().lastLog)
        Log.w(tag, { msg })
        assertEquals("w/$tag $msg", testLogger.get().lastLog)
        Log.e(tag, { msg })
        assertEquals("e/$tag $msg", testLogger.get().lastLog)
        Log.wtf(tag, { msg })
        assertEquals("wtf/$tag $msg", testLogger.get().lastLog)
    }

    @Test
    fun no_info_and_lower_logs_when_info_is_denied_for_tag() {
        Log.deny(tag, LogLevel.INFO)

        Log.d(tag, { msg })
        assertEquals(null, testLogger.get().lastLog)
        Log.i(tag, { msg })
        assertEquals(null, testLogger.get().lastLog)
        Log.w(tag, { msg })
        assertEquals("w/$tag $msg", testLogger.get().lastLog)
        Log.e(tag, { msg })
        assertEquals("e/$tag $msg", testLogger.get().lastLog)
        Log.wtf(tag, { msg })
        assertEquals("wtf/$tag $msg", testLogger.get().lastLog)
    }

    @Test
    fun info_logs_and_higher_are_called_when_info_is_allowed_for_tag() {
        Log.deny(tag, LogLevel.WTF)
        Log.d(tag, { msg })
        assertEquals(null, testLogger.get().lastLog)
        Log.allow(tag, LogLevel.INFO)

        Log.d(tag, { msg })
        assertEquals(null, testLogger.get().lastLog)
        Log.i(tag, { msg })
        assertEquals("i/$tag $msg", testLogger.get().lastLog)
        Log.w(tag, { msg })
        assertEquals("w/$tag $msg", testLogger.get().lastLog)
        Log.e(tag, { msg })
        assertEquals("e/$tag $msg", testLogger.get().lastLog)
        Log.wtf(tag, { msg })
        assertEquals("wtf/$tag $msg", testLogger.get().lastLog)
    }
}