package com.aguragorn.klog

import java.util.logging.Level
import java.util.logging.Logger


actual class PlatformDefaultWriter actual constructor(): LogWriter {
    override fun d(tag: String, msg: () -> String, throwable: Throwable?) {
        val logger = Logger.getLogger(tag)
        logger.log(Level.FINE, throwable, msg)
    }

    override fun i(tag: String, msg: () -> String, throwable: Throwable?) {
        val logger = Logger.getLogger(tag)
        logger.log(Level.INFO, throwable, msg)
    }

    override fun w(tag: String, msg: () -> String, throwable: Throwable?) {
        val logger = Logger.getLogger(tag)
        logger.log(Level.WARNING, throwable, msg)
    }

    override fun e(tag: String, msg: () -> String, throwable: Throwable?) {
        val logger = Logger.getLogger(tag)
        logger.log(Level.SEVERE, throwable, msg)
    }

    override fun wtf(tag: String, msg: () -> String, throwable: Throwable?) {
        // UNSUPPORTED
    }
}