package com.aguragorn.klog

actual class PlatformDefaultWriter actual constructor() : LogWriter {
    override fun d(tag: String, msg: () -> String, throwable: Throwable?) {
        console.log("[${tag}] ${msg()}", throwable?.stackTraceToString())
    }

    override fun i(tag: String, msg: () -> String, throwable: Throwable?) {
        console.info("[${tag}] ${msg()}", throwable?.stackTraceToString())
    }

    override fun w(tag: String, msg: () -> String, throwable: Throwable?) {
        console.warn("[${tag}] ${msg()}", throwable?.stackTraceToString())
    }

    override fun e(tag: String, msg: () -> String, throwable: Throwable?) {
        console.error("[${tag}] ${msg()}", throwable?.stackTraceToString())
    }

    override fun wtf(tag: String, msg: () -> String, throwable: Throwable?) {
        // UNSUPPORTED
    }
}