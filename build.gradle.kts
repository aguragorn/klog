buildscript {
    val kotlinVersion: String by project.properties
    val androidBuildToolsVersion: String by project.properties


    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath("com.android.tools.build:gradle:$androidBuildToolsVersion")
    }
}

plugins {
    kotlin("multiplatform") version "1.8.20"
    id("com.android.library") version "4.2.2"
    id("maven-publish")
}

group = "com.gitlab.aguragorn"
version = "0.3.0"

repositories {
    google()
    mavenCentral()
    gradlePluginPortal()
    maven { url = uri("https://jitpack.io") }
    maven { url = uri("https://oss.sonatype.org/content/repositories/snapshots/") }
}

kotlin {
    android()
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
    }
    js {
        browser {
            testTask {
                enabled = false
            }
        }
        nodejs {
            testTask {
                useMocha()
            }
        }
    }


    sourceSets {
        getByName("commonMain") {
            dependencies {
                implementation("co.touchlab:stately-isolate:1.1.1-a1")
                implementation("co.touchlab:stately-concurrency:1.2.2")
            }
        }
        getByName("commonTest") {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        getByName("jvmMain")
        getByName("jvmTest") {
            dependencies {
                implementation(kotlin("test-junit"))
            }
        }
        getByName("jsMain")
        getByName("jsTest") {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
        val androidMain by getting
        getByName("androidTest") {
            dependsOn(androidMain)
            dependencies {
                implementation(kotlin("test-junit"))
            }
        }
    }
}


android {
    compileSdkVersion(33)
    sourceSets {
        getByName("main") {
            manifest.srcFile("src/androidMain/AndroidManifest.xml")
            java.srcDirs("src/androidMain/kotlin")
            res.srcDirs("src/androidMain/res")
        }
        getByName("test") {
            java.srcDirs("src/androidTest/kotlin")
            res.srcDirs("src/androidTest/res")
        }
    }
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(33)
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

rootProject.plugins.withType<org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootPlugin> {
    rootProject.the<org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootExtension>().nodeVersion = "16.0.0"
}